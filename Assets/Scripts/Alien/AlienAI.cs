﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienAI : MonoBehaviour {

    public float movementSpeed = 10;
    public float distanceToGround = 1;

    Rigidbody2D rb;

    BoxCollider2D bc;
    int groundMask;

    bool isTurned = false;

    ParticleSystem ps;
    
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();

        groundMask = LayerMask.GetMask("Ground");

        ps = GetComponentInChildren<ParticleSystem>();
    }
    
    void Update()
    {
        float distanceToTarget = Vector2.Distance(transform.position, SatelliteManager.instance.transform.position);
        if (distanceToTarget < 2)
            Destroy(gameObject);

        int direction;

        if (SatelliteManager.instance.transform.position.x > transform.position.x)
            direction = 1;
        else
            direction = -1;

        if (direction < 0 && !isTurned)
        {
            transform.localScale = new Vector2(-1, 1);
            isTurned = true;
        }
        else if (direction > 0 && isTurned)
        {
            transform.localScale = new Vector2(1, 1);
            isTurned = false;
        }

        Vector2 vel = rb.velocity;
        
        if(IsGrounded())
        {
            if (!ps.isPlaying)
                ps.Play();
            vel.x = (direction * movementSpeed);
        }

        rb.velocity = vel;
    }

    private bool IsGrounded()
    {
        Vector2 rayStartPosition = transform.position;
        rayStartPosition.y -= (bc.bounds.size.y / 2.5f);

        Debug.DrawRay(rayStartPosition, Vector2.down * distanceToGround, Color.red, 1);

        return Physics2D.Raycast(rayStartPosition, Vector2.down, distanceToGround, groundMask);
    }
}
