﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorShower : MonoBehaviour {

    [Header("Borders")]
    public GameObject leftTopBorder;
    public GameObject leftBottomBorder;
    public GameObject rightTopBorder;
    public GameObject rightBottomBorder;

    [Header("Meteorite")]
    public GameObject meteoritePrefab;

    [Header("Time")]
    [Range(0.1f, 20f)]
    public float time = 2;
    public float minTime = 0.1f;
    public float timeStep = 0.1f;

    [Header("Throw")]
    public float throwStrenghtMin = 5;
    public float throwStrenghtMax = 20;

	void Start () {
        StartCoroutine(InitThrow());
	}

    IEnumerator InitThrow()
    {
        yield return new WaitForSeconds(time);
        if(time > minTime)
        {
            time -= timeStep;
        }

        bool leftSide = (Random.Range(0f, 1f) > 0.5f);

        Vector2 spawnPosition = new Vector2();
        if(leftSide)
        {
            spawnPosition.x = leftTopBorder.transform.position.x;
            spawnPosition.y = Random.Range(leftBottomBorder.transform.position.y, leftTopBorder.transform.position.y);
        }
        else
        {
            spawnPosition.x = rightTopBorder.transform.position.x;
            spawnPosition.y = Random.Range(rightBottomBorder.transform.position.y, rightTopBorder.transform.position.y);
        }

        ThrowMeteorite(spawnPosition, !leftSide);

        StartCoroutine(InitThrow());
    }

    private void ThrowMeteorite(Vector2 position, bool directionToLeft)
    {
        GameObject meteorite = Instantiate(meteoritePrefab, position, new Quaternion(), transform);

        Rigidbody2D rb = meteorite.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(Random.Range(throwStrenghtMin, throwStrenghtMax) * ((directionToLeft) ? -1 : 1), 0f);
    }
}
