﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float movementSpeed = 10;
    public float jumpSpeed = 10;
    public float distanceToGround = 1;

    Rigidbody2D rb;

    BoxCollider2D bc;
    int groundMask;
    int playableItemsLayer;

    Animator animator;
    int speedHash = Animator.StringToHash("speed");
    int jumpHash = Animator.StringToHash("jump");
    int clapIndex;

    bool isTurned = false;
    bool isClapping = false;

    // Use this for initialization
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
        animator = GetComponentInChildren<Animator>();

        groundMask = LayerMask.GetMask("Ground");
        playableItemsLayer = LayerMask.NameToLayer("PlayableItems");

        clapIndex = animator.GetLayerIndex("Clap");
    }

    // Update is called once per frame
    void Update()
    {
        // MOVING
        float horizontalInput = Input.GetAxis("Horizontal");

        if (horizontalInput < 0 && !isTurned)
        {
            transform.localScale = new Vector2(-1, 1);
            isTurned = true;
        }
        else if (horizontalInput > 0 && isTurned)
        {
            transform.localScale = new Vector2(1, 1);
            isTurned = false;
        }

        animator.SetFloat(speedHash, Mathf.Abs(horizontalInput));

        Vector2 vel = rb.velocity;

        //rb.AddForce(transform.right * movementSpeed * horizontalInput);
        vel.x = (transform.right * movementSpeed * horizontalInput).x;

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            //rb.AddForce(transform.up * jumpSpeed);
            vel.y = (transform.up * jumpSpeed).y;
            animator.SetTrigger(jumpHash);
        }

        rb.velocity = vel;

        // CLAP
        if(Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetLayerWeight(clapIndex, 1f);
            isClapping = true;
        }
        else
        {
            animator.SetLayerWeight(clapIndex, 0);
            isClapping = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CheckForAlienFight(collision);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        CheckForAlienFight(collision);
    }

    private void CheckForAlienFight(Collision2D collision)
    {
        if(isClapping && collision.collider.gameObject.layer == playableItemsLayer && collision.collider.CompareTag("Alien"))
        {
            Destroy(collision.collider.gameObject);
        }
    }

    private bool IsGrounded()
    {
        Vector2 rayStartPosition = transform.position;
        rayStartPosition.y -= (bc.bounds.size.y / 2.5f);

        Debug.DrawRay(rayStartPosition, Vector2.down * distanceToGround, Color.red, 1);

        return Physics2D.Raycast(rayStartPosition, Vector2.down, distanceToGround, groundMask);
    }
}
