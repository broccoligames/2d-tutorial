﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreenManager : MonoBehaviour {

    // SINGLETON INSTANCE
    public static EndScreenManager instance;

    public Text scoreDisplay;
    public Text highestScoreDisplay;

    public const string HIGHEST_SCORE_PREF = "HighestScore";

    private CanvasGroup cg;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        cg = GetComponent<CanvasGroup>();

        gameObject.SetActive(false);
    }

    public void ShowEndScreen(float score)
    {
        gameObject.SetActive(true);
    
        scoreDisplay.text = "Vydržel jsi " + GetFormatedScore(score) + "!";

        if(PlayerPrefs.GetFloat(HIGHEST_SCORE_PREF, 0) < score)
        {
            PlayerPrefs.SetFloat(HIGHEST_SCORE_PREF, score);
            highestScoreDisplay.text = "Nový rekord!";
            Debug.Log("New record");
        } 
        else
        {
            highestScoreDisplay.text = "Rekord je " + GetFormatedScore(PlayerPrefs.GetFloat(HIGHEST_SCORE_PREF, 0));
            Debug.Log("No record");
        }

        StartCoroutine(FadeInScreen());
    }

    public string GetFormatedScore(float score)
    {
        return ((int)(score / 60)).ToString("D2") + ":" + (Math.Round((decimal)(score % 60), 2, MidpointRounding.ToEven)).ToString("00.00");
    }

    IEnumerator FadeInScreen()
    {
        if(cg.alpha < 1)
        {
            cg.alpha += 0.01f;
            yield return null;
            StartCoroutine(FadeInScreen());
        }
    }

    // BUTTONS
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    public void ShowLeaderboards()
    {
        // TODO create
        Debug.Log("Not yet implemented!");
    }
}