﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SatelliteManager : MonoBehaviour {

    // SINGLETON INSTANCE
    public static SatelliteManager instance;

    private int health = 100;
    private float score = 0;

    public Text scoreDisplay;

    public Image healthStatus;
    private float healthStatusWidth;

    private ParticleSystem smoke;
    private ParticleSystem.EmissionModule smokeEM;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        healthStatusWidth = healthStatus.rectTransform.rect.width;

        smoke = GetComponentInChildren<ParticleSystem>();
        smokeEM = smoke.emission;
    }

    private void Update()
    {
        score += Time.deltaTime;
        scoreDisplay.text = EndScreenManager.instance.GetFormatedScore(score);

        // ONLY FOR DEVELOPMENT
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.E))
            EndGame();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(health > 0)
        {
            float smokeRate = smokeEM.rateOverTime.constant;

            if (collision.collider.CompareTag("Meteorite")) {
                health -= 10;
                smokeRate += 2;
            } else if (collision.collider.CompareTag("Alien")) {
                health -= 20;
                smokeRate += 4;
            }

            float rightOffset = -(healthStatusWidth - ((healthStatusWidth / 100) * health));
            healthStatus.rectTransform.offsetMax = new Vector2(rightOffset, 0);

            if(health <= 0)
            {
                EndGame();
            }
            if(health <= 30)
            {
                healthStatus.color = Color.red;
            }
            else if(health <= 60)
            {
                healthStatus.color = Color.yellow;
            }
            
            smokeEM.rateOverTime = new ParticleSystem.MinMaxCurve(smokeRate);
            Destroy(collision.collider.gameObject);
        }
    }

    private void EndGame()
    {
        if (EndScreenManager.instance.gameObject.activeSelf)
            return;

        EndScreenManager.instance.ShowEndScreen(score);
        Time.timeScale = 0;
    }
}
