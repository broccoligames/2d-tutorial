# README #

Tento repozitář slouží pro ukládání projektu 2D tutoriálu, který je součástí kroužku/kurzu tvorby her v Unity.
Tutoriál na YouTube můžete najít na adrese https://www.youtube.com/playlist?list=PLk1q6pqAUJjWVPpbah30PxpxaKpcaUR-Y

### Jak to používat? ###

* Na straně Downloads (v levém menu) je možné stáhnou kompletní balíček projektu.
* Nebo pomocí nástroje/software, který podporuje GIT si můžete vytvořit fork tohoto projektu.
* Případně si na internetu nastudujte, jak GIT funguje. Do budoucna se to bude učitě hodit :).